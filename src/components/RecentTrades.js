import React, {Component} from 'react';
import {Table,Card,CardHeader,CardTitle,CardBody} from 'reactstrap'
import Binance from 'binance-api-node';

export default class RecentTrades extends Component {

    constructor(props){
        super(props);
        this.state = {
            tradelist: [],
        }
        this.ids = [];
        this.clean = null;
    }

    componentDidMount(){
        this.clean = Binance().ws.trades([this.props.pair], trade => {
            let tradelist = this.state.tradelist;
            tradelist.length <= 21 || tradelist.pop();    
            tradelist.push(trade);
            this.setState({tradelist});
        });
    }

    componentWillUnmount(){
        typeof this.clean === "function" && this.clean();
    }

    render(){

        return(
            <Card>
                <CardHeader>
                    <CardTitle tag="h4" className="mb-0">
                        Recent Trades {this.props.pair}
                    </CardTitle>
                </CardHeader>
                <CardBody className="p-0">
                    <Table dark size="sm" className="mb-0">
                        <thead>
                            <tr>
                                <th>Amount</th>
                                <th>Price</th>
                                <th>Sum</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                           this.state.tradelist.map(trade => {
                            return (
                                <tr key={trade.tradeId + ''}>
                                    <td>{trade.quantity}</td>
                                    <td>{trade.price}</td>
                                    <td>{parseFloat(trade.quantity) * parseFloat(trade.price)}</td>
                                </tr>
                            )
                            })
                        }
                        </tbody>
                   </Table>
                </CardBody>
            </Card>
        );

    }
}