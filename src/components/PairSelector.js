import React, {Component} from 'react';
import {Table,Card,CardHeader,CardTitle,CardBody, Button} from 'reactstrap'
import Binance from 'binance-api-node';
import {BigNumber} from 'bignumber.js';


export default class PairSelector extends Component {

    constructor(props){
        super(props);
        this.state = {
            message: "",
            pairs: {}
        }
        this.pairSelect = this.pairSelect.bind(this);
    }

    pairSelect(e,pair = false){
        console.log("this one is for free");
        pair = pair || e.target.value;
        this.setState({message: <p>you selected <strong>{pair}</strong> pass this to the other components.</p>})
    }

    componentDidMount(){
        this.clean = Binance().ws.allTickers(tickers => {
            let pairs = {};
            tickers.some((ticker,idx) => {
                if(idx > 20) return true; 
                pairs[ticker.symbol] = {
                    change: new BigNumber(ticker.priceChangePercent),
                    ticker: new BigNumber(ticker.bestAsk).plus(ticker.bestBid).dividedBy(2),
                    volume: new BigNumber(ticker.volume)
                }
                return false;
            });
            this.setState({pairs});
        })
    }

    componentWillUnmount(){
        typeof this.clean === "function" && this.clean();
    }


    render(){

        return(
            <Card>
                <CardHeader>
                <CardTitle tag="h4" className="mb-0">
                        Trading pairs
                    </CardTitle>
                </CardHeader>
                <CardBody className="p-0">
                    {this.state.message}
                <Table dark size="sm" className="mb-0"> 
                        <thead>
                            <tr>
                                <th>idx</th>
                                <th>Pair</th>
                                <th>Price</th>
                                <th>Vol</th>
                                <th>Change</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            Object.keys(this.state.pairs).map((pair,idx) => {
                                return (
                                <tr key={'pair-' + pair}>
                                <td>{idx}</td>
                                    <td>
                                        <Button className="p-0" color="link" onClick={(e) => { this.pairSelect(e,pair)}}>
                                            {pair}
                                        </Button>
                                    </td>
                                    <td>
                                        {this.state.pairs[pair].ticker.toNumber()}
                                    </td>
                                    <td>
                                        {this.state.pairs[pair].volume.toNumber()}
                                    </td>
                                    <td>
                                        {this.state.pairs[pair].change.toNumber()}
                                    </td>
                                </tr>
                                )
                            })
                        }
                        </tbody>
                   </Table>
                </CardBody>
            </Card>
        );

    }
}